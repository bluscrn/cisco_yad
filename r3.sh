#!/bin/bash
#~~~~~~~~~~~~~~~~~~~~
#Source our variables
#~~~~~~~~~~~~~~~~~~~~
source IP.var
source serial.var
#~~~~~~~~~~~~~~~~~~~~
#Configure the device
#~~~~~~~~~~~~~~~~~~~~
(
echo -e '\r' > $serial
echo -e 'no' > $serial
sleep 15
echo "25"
echo -e '\r' > $serial
echo -e 'enable' > $serial
echo -e 'config t' > $serial
echo -e 'interface f0/1' > $serial
echo -e 'no shutdown' > $serial
echo -e 'ip address '${IP%.*}'.9 255.255.255.0' > $serial
echo -e 'end' > $serial
echo -e 'write' > $serial
echo "50"
echo -e 'ping '${IP}' repeat 100' > $serial
sleep 21
echo "75"
sleep 21
echo -e 'copy tftp://'${IP}'/c5915-adventerprisek9-mz.SPA.159-3.M.bin flash:' > $serial
echo -e '\r' > $serial
echo "100"
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#Progress bar...Really just wanted a warning not to remove the cable until complete
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
) |
yad --progress \
  --title="Warning!!! DO NOT remove console cable" \
  --text="Running...DO NOT remove cable!" \
  --percentage=0 \
  --auto-close \
  --width=600 \
  --height=5

if [ "$?" = -1 ] ; then
        yad --error \
          --text="Update canceled."
fi
