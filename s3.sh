#/bin/bash
#~~~~~~~~~~~~~~~~~~~~
#Source our variables
#~~~~~~~~~~~~~~~~~~~~
source IP.var
source serial.var
#~~~~~~~~~~~~~~~~~~~~
#Configure the device
#~~~~~~~~~~~~~~~~~~~~
(
echo -e '\r' > $serial
echo -e 'no' > $serial
echo -e 'enable' > $serial
echo -e 'config t' > $serial
echo -e 'interface vlan 1' > $serial
echo -e 'no shutdown' > $serial
echo -e 'ip address '${IP%.*}'.3 255.255.255.0' > $serial
echo "25"
echo -e 'interface GigabitEthernet1/1' > $serial
echo -e 'no shutdown' > $serial
echo -e 'interface GigabitEthernet1/2' > $serial
echo -e 'no shutdown' > $serial
echo "50"
echo -e 'interface F1/8' > $serial
echo -e 'no shutdown' > $serial
echo -e 'switchport access vlan 1'
echo -e 'end' > $serial
echo -e 'write' > $serial
echo -e 'ping '${IP}' repeat 100' > $serial
echo "75"
sleep 10
echo -e 'copy tftp://'${IP}'/c2020-universalk9-mz.152-4.EA8.bin flash:' > $serial
echo -e '\r' > $serial
echo "100"
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#Progress bar...Really just wanted a warning not to remove the cable until complete
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
) |
yad --progress \
  --title="Warning!!! DO NOT remove console cable" \
  --text="Running...DO NOT remove cable!" \
  --percentage=0 \
  --auto-close \
  --width=600 \
  --height=5

if [ "$?" = -1 ] ; then
        yad --error \
          --text="Update canceled."
fi
